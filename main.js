const selects = document.querySelectorAll("select");
const items = document.querySelectorAll("li[data-carbon]");
function sumUpCarbon() {
    let sum = 0;
    for (let s of selects) {
        sum += parseInt(s.selectedOptions[0].getAttribute("data-carbon"));
    }
    for (let i of items) {
        sum += parseInt(i.getAttribute("data-carbon"));
    }
    document.querySelector("#total-counter").innerText = `Total carbon: ${sum}`;
}

const options = document.querySelectorAll("option[data-carbon]");
for (let option of options) {
    option.innerHTML = `${option.getAttribute("data-carbon")} - ${option.textContent}`;
}

for (let item of items) {
    item.innerHTML = `${item.getAttribute("data-carbon")} - ${item.innerHTML}`;
}

const form = document.querySelector("form");
for (let select of selects) {
    select.addEventListener("change", sumUpCarbon);
}

sumUpCarbon();
